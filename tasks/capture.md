# General

- dedicated capture computer?
- solve all audio and resource issues

# Posts (xsplit)

- xsplit automation scripts
- OSC / Midi Listners for record buttons
- global record or local?
- if local -> macro: focus window, do command, focus back
- midi translate -> keystrokes

# Streams (obs)

- recording hotkey
- websockets: https://github.com/Palakis/obs-websocket
-
