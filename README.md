# Elijah Noodles

elijah's open source music project.

# Ideas

- posting songs based on community-driven prompts

- xsplit automation
- footpedal capture
- midi translate
- capture workflow reel
- auto video record when audio is recording
- chat -> sfx (pop sound)
- chat -> midi / osc soundboard?
- chat -> xsplit

# Studio One Presets

- show

# Content sources

## Posts

- open source video games

## Streams

-

# Needs

- some sort of web app
- catalog of posted shit, insta videos, and other platforms
- todo list website thingy
- elijahnoodles.com
- list of prompts (with community voting system?)
- moderation & reporting because douchebags exist
- later
- single sign on with facebook / google / twitch / patreon (for subs)

# Support

- patreon -> https://patreon.com/elijahlucian

# System

- list of hashtags based on popularity (if that even matters anymore)
-

# Help

- when asking for help, propose incorrect solutions
