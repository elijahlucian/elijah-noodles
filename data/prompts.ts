type Prompt = [
  body: string,
  name: string,
  source: "instagram" | "twitter",
  result?: string,
  at?: number
]

export const prompts: Prompt[] = [
  [
    "Don't Touch My Fries",
    "missyjo_pinup",
    "instagram",
    "https://www.instagram.com/p/CMFuucjFsbe/",
    1615068366168,
  ],
  ["Pants", "mysterydip", "twitter"],
]

const item = prompts[0]
